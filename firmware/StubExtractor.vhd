----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2020 05:11:58 PM
-- Design Name: 
-- Module Name: StubExtractor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.constants.all;

entity StubExtractor is
    port (
        clk : in std_logic;
        data_in : in std_logic_vector(word_width - 1 downto 0) := (others => '0')
--        reset : in std_logic := '0';
--        data_out : out std_logic_vector(stub_width - 1 downto 0) := (others => '0')
    );
end StubExtractor;

architecture Behavioral of StubExtractor is
    signal bit_count : integer := 0;
    signal remaining_bits : integer := 0;
    signal header_complete : std_logic := '0';
    signal stubs_complete : std_logic := '0';
    signal stub_index : integer := 0;
    
    signal header : std_logic_vector(header_width - 1 downto 0) := (others => '0');
    signal stubs : tStubArray := NullStubArray;
begin
    pMain : process(clk)
    begin
        if rising_edge(clk) then
            if header_complete = '0' then
                if bit_count < header_width then
                    if bit_count > (header_width - word_width) then
                        header(header_width - 1  downto bit_count) <= data_in(header_width - bit_count - 1 downto 0);
                        remaining_bits <= word_width - (header_width - bit_count);
                        header_complete <= '1';
                        stubs(0)(word_width - (header_width - bit_count) - 1 downto 0) <= data_in(word_width - 1 downto header_width - bit_count);
                        bit_count <= word_width - (header_width - bit_count);
                    else
                        header(bit_count + word_width - 1 downto bit_count) <= data_in;
                        bit_count <= bit_count + word_width;
                    end if;
                end if;
            else
                if stubs_complete = '0' then
                    if bit_count < stub_width then
                        if bit_count >= (stub_width - word_width) then
                            stubs(stub_index)(stub_width - 1 downto bit_count) <= data_in(stub_width - bit_count - 1 downto 0);
                            remaining_bits <= word_width - (stub_width - bit_count);
                            stub_index <= stub_index + 1;
                            bit_count <= word_width - (stub_width - bit_count);
                            if stub_index = (max_number_of_stubs - 1) then
                                stubs_complete <= '1';
                            else
                                stubs(stub_index + 1)(word_width - (stub_width - bit_count) - 1 downto 0) <= data_in(word_width - 1 downto stub_width - bit_count);
                            end if;
                        else
                            stubs(stub_index)(bit_count + word_width - 1 downto bit_count) <= data_in;
                            bit_count <= bit_count + word_width;
                        end if;
                    end if;
                end if;
            end if;            
        end if;
    end process pMain;


end Behavioral;
