----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2020 10:51:01 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.constants.all;

entity top is
    port (
        clk : in std_logic;
        data_in : in std_logic_vector(gbt_frame_width - 1 downto 0) := (others => '0');
        data_valid : in std_logic := '0';
        data_out : out std_logic_vector(64 - 1 downto 0) := (others => '0')
    );
end top;

architecture Behavioral of top is
    constant period : integer := 10;
    signal counter : integer := 0;
    signal word : std_logic_vector(64 - 1 downto 0) := (others => '0');
begin
    GBTFramerInstance : entity work.GBTFramer
    PORT MAP(
        -- Input Ports --
        clk => clk,
        data_in => data_in,
        data_valid => data_valid,
        -- Output Ports --
        data_out => word
    );
    
    StubExtractorInstance : entity work.StubExtractor
    PORT MAP(
        clk => clk,
        data_in => word(word_width -1 downto 0)
    );
    
    data_out <= word;
end Behavioral;
