library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package constants is
    constant gbt_frame_width : integer := 112;
    constant number_of_e_links : integer := 5;
    constant number_of_cics : integer := 2;
    constant word_width : integer := number_of_e_links;
    
    constant stub_width : integer := 18;
    constant header_width : integer := 28;
    constant max_number_of_stubs : integer := 16;
    
    constant data_period : integer := 8;
    
    type tUnboundedIntegerArray is array(integer range <>) of integer;
    subtype tLinkIndicesArray is tUnboundedIntegerArray(0 to number_of_e_links - 1);
    subtype tL1AIndicesArray is tUnboundedIntegerArray(0 to number_of_cics - 1);
    type tUnbounded2DIntegerArray is array(integer range <>) of tLinkIndicesArray;
    subtype tMultiCICLinkIndicesArray is tUnbounded2DIntegerArray(0 to number_of_cics - 1);
    
    constant link_indices : tMultiCICLinkIndicesArray := ((96, 32, 80, 8, 88), (56, 104, 24, 72, 16));
    constant link_offsets : tMultiCICLinkIndicesArray := ((2, -1,  0,  1, -2), (2,  1,   0,   1, -2));
    constant l1a_indices  : tL1AIndicesArray          := (40, 64);
    
    type tUnboundedLinkArray is array(integer range <>) of std_logic_vector(7 downto 0);
    subtype tLinkArray is tUnboundedLinkArray(number_of_e_links - 1 downto 0);
    constant NullLinkArray : tLinkArray := (others => (others => '0'));
    
    type tUnboundedMultiCICLinkArray is array(integer range <>) of tLinkArray;
    subtype tMultiCICLinkArray is tUnboundedMultiCICLinkArray(0 to number_of_cics - 1);
    constant NullMultiCICLinkArray : tMultiCICLinkArray := (others => NullLinkArray);
    
    type tUnboundedStubArray is array(integer range <>) of std_logic_vector(stub_width - 1 downto 0);
    subtype tStubArray is tUnboundedStubArray(max_number_of_stubs - 1 downto 0);
    constant NullStubArray : tStubArray := (others => (others => '0'));
end package constants;
