----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2020 11:14:47 AM
-- Design Name: 
-- Module Name: GBTFramer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.constants.all;
use work.emp_data_types.all;

entity GBTFramer is
  Port (
    clk : in std_logic; --- 320MHz
    data_in : in std_logic_vector(gbt_frame_width - 1 downto 0) := (others => '0'); --- GBT Frame every 8 clks, i.e. 40MHz effective
    data_valid : in std_logic := '0'; --- Enable signal expected to go high for 1 clk every 8 clks
    data_out : out std_logic_vector(64 - 1 downto 0) := (others => '0')
  );
end GBTFramer;

architecture Behavioral of GBTFramer is
    signal stub_links : tMultiCICLinkArray := NullMultiCICLinkArray;
    signal previous_stub_links : tMultiCICLinkArray := NullMultiCICLinkArray;
    signal new_data : std_logic := '0';
       
    signal previous_data : std_logic_vector(gbt_frame_width - 1 downto 0) := (others => '0');
       
    signal stream_valid : std_logic := '0';
    signal time_index : integer := 0;
begin

    pMain : process(clk)
    begin
        if rising_edge(clk) then
            --- Link routing and alignment ---
            if data_valid = '1' then
                stream_valid <= '1';
                for i in 0 to number_of_cics - 1 loop
                    for j in 0 to number_of_e_links - 1 loop
                        if link_offsets(i)(j) = 0 then
                            stub_links(i)(j)(7 downto 0) <= data_in(link_indices(i)(j) + 7 downto link_indices(i)(j));
                            previous_stub_links(i)(j) <= stub_links(i)(j);
                        elsif link_offsets(i)(j) > 0 then
                            stub_links(i)(j)(7 - link_offsets(i)(j) downto 0) <= data_in(link_indices(i)(j) + 7 downto link_indices(i)(j) + link_offsets(i)(j));
                            previous_stub_links(i)(j)(7 downto 7 - (link_offsets(i)(j) - 1)) <= data_in(link_indices(i)(j) + (link_offsets(i)(j) - 1) downto link_indices(i)(j));
                            previous_stub_links(i)(j)(7 - link_offsets(i)(j) downto 0) <= stub_links(i)(j)(7 - link_offsets(i)(j) downto 0);
                        elsif link_offsets(i)(j) < 0 then 
                            stub_links(i)(j)(7 downto -link_offsets(i)(j)) <= data_in(link_indices(i)(j) + 7 + link_offsets(i)(j) downto link_indices(i)(j));
                            stub_links(i)(j)(-link_offsets(i)(j) - 1 downto 0) <= previous_data(link_indices(i)(j) + 7 downto link_indices(i)(j) + 7 - (-link_offsets(i)(j) - 1));
                            previous_stub_links(i)(j) <= stub_links(i)(j);
                        end if;                    
                    end loop;
                end loop;
                time_index <= 0;
                previous_data <= data_in;
            end if;
            --- Transpose link array ---
            if stream_valid = '1' then
                if time_index = data_period - 1 then
                    if data_valid = '0' then
                        stream_valid <= '0';
                    end if;
                else
                    time_index <= time_index + 1;
                end if;
                
                for i in 0 to number_of_cics -1 loop
                    for j in 0 to word_width - 1 loop
                        data_out(32*i + j) <= previous_stub_links(i)(j)(time_index);
                        --- Stream L1A link ---
                        data_out(32*i + word_width) <= data_in(l1a_indices(i) + time_index);           
                    end loop;
                end loop;
            else
                data_out <= (others => '0');
            end if;
        end if;
    end process pMain;

end Behavioral;
