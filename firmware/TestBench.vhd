----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 03:44:40 PM
-- Design Name: 
-- Module Name: TestBench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.constants.all;


entity TestBench is
end TestBench;

architecture Behavioral of TestBench is
    signal clk : std_logic := '0';
    signal counter : integer := 0; -- Counter to more easily calculate latency
    -- signal valid_counter : integer := 0;
    signal stream_in : std_logic_vector(gbt_frame_width - 1 downto 0) := (others => '0');
    signal data_out : std_logic_vector(64 - 1 downto 0) := (others => '0');
    signal data_valid : std_logic := '0';
begin

    clk <= not clk after 12.5 ns;

    -- Process for increasing counter by one each clock cycle
    process(clk)
    begin
        if rising_edge(clk) then
            counter <= counter + 1;
        end if;
    end process;
    
    pDataValid: process(clk)
    variable valid_counter : integer := 0;
    begin
        if rising_edge(clk) then
            if valid_counter = data_period - 1 then
                valid_counter := 0;
            else
                valid_counter := valid_counter + 1;
            end if;
            if valid_counter = 0 then
                data_valid <= '1';
            else
                data_valid <= '0';
            end if;
        end if;
    end process pDataValid;


    SourceGeneratorInstance : entity work.SourceGenerator
    port map (
        clk => clk,
        data_out => stream_in,
        data_in => data_out
    );

    AlgoInstance : entity work.top
    PORT MAP(
        clk => clk,
        data_in => stream_in,
        data_valid => data_valid,
        
        data_out => data_out
    );

end Behavioral;