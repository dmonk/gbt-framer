#!/usr/bin/env python
# coding: utf-8

from gbtframer import GbtFramer
from stubextractor import StubExtractor
from testbenchio import SourceGenerator


# Header separation
# fe_type  = words[0][0]
# status   = words[0][1:8] + words[1]
# bcid     = words[2]      + words[3] + words[4][0:2]
# no_stubs = words[4][2:8] + words[5][0:3]

def main():
    sg = SourceGenerator()
    sg.constructGBTAlignmentWord()
    sg.write("../firmware/alignment.txt")

    number_of_e_links = 5
    gbtf = GbtFramer(
        number_of_e_links=number_of_e_links,
        gbt_input_width=112
    )
    gbtf.generateInput(sg.word)
    word_stream = gbtf.generateOutputStream()
    print([i.toHex(64) for i in word_stream])

    se = StubExtractor(
        word_width=number_of_e_links,
        header_width=28,
        stub_width=18,
        max_number_of_stubs=16
    )
    se.loadStream(word_stream)
    boxcar = se.extract()
    print(boxcar.header)
    for stub in boxcar.stubs:
        print(stub)


if __name__ == '__main__':
    main()
