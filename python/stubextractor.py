from datatypes import BoxCar


class StubExtractor:
    def __init__(self, **kwargs):
        self.verbose = False
        self.__dict__.update(kwargs)

    def loadStream(self, stream):
        self.stream = stream
        for word in self.stream:
            assert len(word) == self.word_width

    def extract(self):
        boxcar = BoxCar(
            self.header_width, self.stub_width, self.max_number_of_stubs)
        bit_count = 0
        remaining_bits = 0
        header_complete = False
        stubs_complete = False
        stub_index = 0
        for index, word in enumerate(self.stream):
            if self.verbose:
                print("Word #%d Value: %s" % (index, word))
            if header_complete is not True:
                if bit_count < self.header_width:
                    if bit_count > (self.header_width - self.word_width):
                        boxcar.header[bit_count:self.header_width] = word[0:self.header_width - bit_count]
                        remaining_bits = self.word_width - (self.header_width - bit_count)
                        header_complete = True
                        boxcar.stubs[0][0:remaining_bits] = word[self.header_width - bit_count:self.word_width]
                        bit_count = remaining_bits
                    else:
                        boxcar.header[bit_count:bit_count + self.word_width] = word
                        bit_count += self.word_width
            elif stubs_complete is not True:
                if bit_count < self.stub_width:
                    if bit_count >= (self.stub_width - self.word_width):
                        boxcar.stubs[stub_index][bit_count:self.stub_width] = word[0:self.stub_width - bit_count]
                        remaining_bits = self.word_width - (self.stub_width - bit_count)
                        stub_index += 1
                        if stub_index == self.max_number_of_stubs:
                            stubs_complete = True
                        else:
                            boxcar.stubs[stub_index][0:remaining_bits] = word[self.stub_width - bit_count:self.word_width]
                        bit_count = remaining_bits
                    else:
                        boxcar.stubs[stub_index][bit_count:bit_count + self.word_width] = word
                        bit_count += self.word_width
            if self.verbose:
                print("bit_count = %d remaining_bits = %d" % (bit_count, remaining_bits))
        return boxcar
