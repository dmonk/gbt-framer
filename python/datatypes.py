class SLV:
    def __init__(self, length):
        self.length = length
        self.data = [0 for i in range(self.length)]

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value

    def __str__(self):
        return "".join([str(i) for i in self.data])

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return self.length

    def assign(self, data):
        assert(len(data) == len(self))
        for i in data:
            assert(type(i) == int)
        self.data = data

    def toHex(self, bit_length=None):
        assert max(self.data) <= 1, "Max value of data variable = {}".format(
            max(self.data))
        if bit_length is None:
            width = len(self.data)
        else:
            width = bit_length
        return "{:0{width}x}".format(
            int("".join([str(i) for i in self.data][::-1]), 2),
            width=int(width/4))


class BoxCar:
    def __init__(self, header_width, stub_width, max_number_of_stubs):
        self.header = SLV(header_width)
        self.stubs = [SLV(stub_width) for i in range(max_number_of_stubs)]
