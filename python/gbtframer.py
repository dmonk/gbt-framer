from numpy import random
from datatypes import SLV


ALIGNMENT_PATTERN = [1, 1, 1, 0, 1, 0, 1, 0]
ALIGNMENT_WORD = [
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 0, 1, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 0, 1, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 0, 1, 0, 1, 0,
    1, 1, 1, 0, 1, 0, 1, 0,
    1, 1, 1, 0, 1, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
]


class GbtFramer:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def generateInput(self, word=ALIGNMENT_WORD):
        input_stream = [SLV(self.gbt_input_width) for i in range(8)]
        for i in range(len(input_stream)):
            input_stream[i].assign(
                # [
                #     i*int(random.choice([1])) for j in range(
                #         self.gbt_input_width)
                # ]
                word
            )
        self.input_stream = input_stream
        return input_stream

    def mapLinks(self, input):
        stub_links = [SLV(8) for i in range(self.number_of_e_links)]
        stub_links[0].assign(input[96:103+1])
        stub_links[1].assign(input[32:39+1])
        stub_links[2].assign(input[80:87+1])
        stub_links[3].assign(input[8:15+1])
        stub_links[4].assign(input[88:95+1])

        for link in stub_links:
            assert(len(link) == 8)

        return stub_links

    def transpose(self, links):
        words = [SLV(self.number_of_e_links) for i in range(8)]
        for i in range(8):
            for j in range(self.number_of_e_links):
                words[i][j] = links[j][i]
        return words

    def generateOutputStream(self):
        stream = []
        for frame in self.input_stream:
            stream += self.transpose(self.mapLinks(frame))
        return stream
