from datatypes import SLV


class SourceGenerator:
    def __init__(self, word_width=128):
        self.length = word_width
        self.word = "".join(['0' for i in range(word_width)])

    def constructGBTAlignmentWord(self):
        alignment_byte = SLV(8)
        alignment_byte.assign([1, 1, 1, 0, 1, 0, 1, 0])
        alignment_word = SLV(112)
        alignment_word[96:103+1] = alignment_byte
        alignment_word[32:39+1] = alignment_byte
        alignment_word[80:87+1] = alignment_byte
        alignment_word[8:15+1] = alignment_byte
        alignment_word[88:95+1] = alignment_byte
        self.word = alignment_word
        return self.word

    def write(self, filename, number_of_lines=100):
        with open(filename, "w") as f:
            for i in range(number_of_lines):
                f.write(self.word.toHex(bit_length=self.length) + "\n")
